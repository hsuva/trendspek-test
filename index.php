<?php
  require __DIR__ . '/vendor/autoload.php';

if (file_exists(__DIR__.'/.env')) {
    // Load environment variables.
    $dotenv = Dotenv\Dotenv::create(__DIR__);
    $dotenv->load();
}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="/node_modules/cesium/Build/Cesium/Cesium.js"></script>
    <link href="/node_modules/cesium/Build/Cesium/Widgets/widgets.css" rel="stylesheet">
    <style>
        #measuretoolbar {
          position: absolute;
          top : 700px;
          right: 30px;
          display: inline;
          margin: 10px;
          padding: 0px;
          background: #394148;
        }
    </style>

    <title>TrendSpek Technical Test: Backend Mode</title>
  </head>
  <body>
    <div class="container">
        <h1>Welcome to the TrendSpek Technical Test: Backend Mode!</h1>
        <div class="row mb-5 mt-5">
            <div class="col-md-12">
                <form class="form-inline">
                    <input id="location" name="location" type="text" class="form-control mb-2 mr-sm-2" style="display: none" placeholder="Enter a location" value="">

                    <button id="go" type="button" class="btn btn-primary mb-2 mr-sm-2">Go!</button>
                    <button id="save" type="button"  class="btn btn-primary mb-2 mr-sm-2">Save default location.</button>
                </form>
                <div id="cesiumContainer"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="requirements">
                    <h2>Requirements</h2>

                    <p>You'll need to have the following installed on your computer already:</p>

                    <ul>
                        <li>PHP 7+</li>
                        <li>npm</li>
                        <li>Composer</li>
                    </ul>
                </div>

          <div class="instructions">
            <h2>Instructions</h2>

            <ol class="instructions-1">
              <li><code>npm install</code></li>
              <li><code>composer install</code></li>
              <li>Copy <code>.env.example</code> to <code>.env</code>. You will need this file shortly.</li>
              <li><code>php -s localhost:8000</code></li>
              <li>Open this in your browser: <code>http://localhost:8000</code></li>
              <li>Create a Cesium ion account <a href="https://cesium.com/ion/signup?gs=true" target="_blank">Sign up here<sup><span class="fa fa-external-link "></span></sup></a> to get a token.</li>
              <li>Update this file with your Cesium token in the obvious location (see step 3)</li>
              <li>Refresh this page.</li>
            </ol>

            <div class="instructions-2" style="display:none;">
              <p>
                Below is Cesium, which is a Javascript library we use for looking at details on a map.
                You can interact with the globe using you mouse, by dragging, clicking, and right-clicking.
              </p>
              <p>
                Below is a very basic form. Clicking on the <b>Go</b> button will take you to Mt Everest. You will notice the coordinates are hard-coded into the PHP code below.
              </p>
              <p>
                Clicking the Save button currently does nothing.
              </p>
              </p>
              <p>
                To complete this test, you will need to write a very basic PHP backend API so save a location and load a location.
                To do the bonus question (#3), you'll need to connect to a third-party API for geolocation, and populate this here.
              </p>
              <h3>Task 1.</h3>
              <ol>
                <li>Write a basic PHP backend at <code>location.php?default</code> that serves cartesian coordinates.</li>
                <li>Update the JQuery so that this loads <code>lookAtLocation()</code> properly.</li>
                <li>Set the default location to anywhere in the world you fancy!</li>
              </ol>

              <h3>Task 2.</h3>
              <ol>
                <li>Add persistence--implement the Save button, sending the current Cesium location to the backend (location.php)</li>
                <li>Extend location.php to save the location, and update <code>location.php?default</code> to serve these coordinates.</li>
                <li>You <i>can</i> use a DB if you would like; or store it in a file; or the $_SESSION, or whatever you choose. <b>But</b>, please store it via PHP.</li>
              </ol>

              <h3>Task 3.</h3>
              <ol>
                <li>Add geolocation!</li>
                <li>Allow a user to enter a location into the <code>location</code> text field.</li>
                <li>If this field is populated, connect to a geolocation API (you can choose one; or use Google Maps Geolocation API).</li>
                <li>When the user clicks 'Go', get this location, and pass it into Cesium so that Cesium window updates.</li>
              </ol>
            </div>
          </div>
        </div>
      <hr>

      </div>
    </div>

    <script>
        Cesium.Ion.defaultAccessToken = "<?php echo getenv('CESIUM_ACCESS_TOKEN') ?>";
    </script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/node_modules/jquery/dist/jquery.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoFI7eIERSrs8OYA7iDpSUL20LPZ-fKWE&libraries=places"></script>
    <script src="/node_modules/bootstrap/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script>

        var viewer = new Cesium.Viewer('cesiumContainer');

        if (Cesium.Ion.defaultAccessToken) {
            $('.instructions-1').hide();
            $('.instructions-2').show();
            $('#interaction').show();
        }

        // var center = Cesium.Cartesian3.fromDegrees(-82.5, 35.3);

        var g_target = [300770.50872389384, 5634912.131394585, 2978152.2865545116];
        var g_offset = [6344.974098678562, -793.3419798081741, 2499.9508860763162];

        function lookAtLocation() {
            var target = new Cesium.Cartesian3(...g_target);
            var offset = new Cesium.Cartesian3(...g_offset);

            viewer.camera.lookAt(target, offset);
            viewer.camera.lookAtTransform(Cesium.Matrix4.IDENTITY);

            $('#location').show();
        }

      $(document).ready(function() {

        $('#go').click(function(e){
            e.preventDefault();

            if($('#location').is(':hidden')) {
              lookAtLocation();
              return;
            }

            var text = $('#location').val();

            if(text.length) {
              var geocoder = viewer.geocoder.viewModel;

              geocoder.searchText = text;
              geocoder.flightDuration = 0.0;
              geocoder.search();
            } else {
              alert('Please enter your location');
            }
        });

        $('#save').click(function(e) {
            e.preventDefault();

            var camera = viewer.scene.camera;
            var currentCartesian = camera.position.clone();

            $.post('location.php?default', {
                'x': currentCartesian.x,
                'y': currentCartesian.y,
                'z': currentCartesian.z
            })
            .done(function (data) {
                console.log('Successfully update default location. [' + data + ']');
            });
        });



        $.get({
            method: 'get',
            dataType: 'json',
            url: '/location.php',
            success: function(response) {
                if (response != null) {
                    g_target = response.target;
                    lookAtLocation();
                }
                console.log(response);
            },
            fail: function() {
                // Note, these affect the global variables above.
                alert('Error! Was not able to find a coordinte. Location was set to default.');
                // g_target = [300770.50872389384, 5634912.131394585, 2978152.2865545116];
                // g_offset = [6344.974098678562, -793.3419798081741, 2499.9508860763162];
            }
        });


        var input = document.getElementById('location');
        var options = {};
        autocomplete = new google.maps.places.Autocomplete(input, options);
      });

    </script>
  </body>
</html>