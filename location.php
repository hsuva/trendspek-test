<?php

if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') die('Invalid request.');

$file = 'default.txt';

$default = json_decode(file_get_contents($file), true);

$target = isset($default["target"]) ? $default["target"] : '';

if ($_POST && isset($_REQUEST['default'])) {
    $x = floatval($_POST['x']);
    $y = floatval($_POST['y']);
    $z = floatval($_POST['z']);
    $default['target'] = [$x, $y, $z];

    file_put_contents($file, json_encode($default));
}

echo json_encode($default);
?>